//
//  CustomCell.swift
//  faeapp
//
//  Created by julian on 17/06/2019.
//  Copyright © 2019 julian. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell {
    var title : String?
    var mainImage : UIImage?
    
    var descr : String?
    
    var date : String?

    
    var titleView : UITextView = {
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        textView.font = UIFont.boldSystemFont(ofSize: 23)
        textView.textColor = UIColor(red:0.51, green:0.30, blue:0.59, alpha:1.0)
        return textView
    }()
    
    var mainImageView : UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    var descrView : UITextView = {
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        //textView.font = UIFont.boldSystemFont(ofSize: 15)
        //textView.textColor = UIColor(red:0.51, green:0.30, blue:0.59, alpha:1.0)
        return textView
    }()
    
    var dateView : UITextView = {
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.isScrollEnabled = false
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        textView.textColor = UIColor(red:0.51, green:0.30, blue:0.59, alpha:1.0)
        //textView.textColor = UIColor(red:0.51, green:0.30, blue:0.59, alpha:1.0)
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(mainImageView)
        self.addSubview(titleView)
        self.addSubview(descrView)
        self.addSubview(dateView)
        
        mainImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        mainImageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        mainImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        mainImageView.bottomAnchor.constraint(equalTo: self.titleView.topAnchor).isActive = true
        mainImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        
       
      
        
        
        
        
        
        
        
        
        
        
        
        titleView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        titleView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        titleView.bottomAnchor.constraint(equalTo: self.descrView.topAnchor).isActive = true
        descrView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        descrView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        descrView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        

       

        /*
        descrView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        descrView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        descrView.bottomAnchor.constraint(equalTo: self.titleView.bottomAnchor).isActive = true
        */
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let title = title {
            titleView.text = title


        }
        if let image = mainImage {
            mainImageView.image = image
            
        }
        if let descr = descr {
            descrView.text = descr
            
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) no ha sido implementado")
    }
    
}
