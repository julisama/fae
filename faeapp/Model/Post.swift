//
//  Post.swift
//  faeapp
//
//  Created by julian on 17/06/2019.
//  Copyright © 2019 julian. All rights reserved.
//

import Foundation
class Post {
    var name : String
    var description : String
    var offer : String
    var address : String
    var date : String
    var photoUrl : String
    
    init(nameText: String, descriptionText: String, offerText: String, addressText: String, dateText: String, photoUrlText: String) {
        name = nameText
        description = descriptionText
        offer = offerText
        address = addressText
        date = dateText
        photoUrl = photoUrlText
    }
}
